#include <iostream>
using namespace std;

const char* StrStr(const char* str1,const char* str2){
	if(str2 == NULL){
		return NULL;
	}
	const char* p = str1;
	while(*p){
		const char* p1 = p;
		const char* p2 = str2;
		while(*p1 && *p2 && (*p1==*p2)){
			p1++;
			p2++;
		}
		if(! *p2){
			return p;
		}
		p++;
	}
	return NULL;
}
int StrLen(const char* str){
	int i = 0;
	const char* p = str;
	while(*p){
		p++;
		i++;
	}
	return i;
}
int  StrStrForLoop(const char* str1,const char* str2){
	int str1_len = StrLen(str1);
	int str2_len = StrLen(str2);
	int max_len = str1_len - str2_len;
	for(int i = 0;i <= max_len;i++){
		int match_len = 0;
		while(str1[i+match_len] == str2[match_len]	\
			&& match_len < str2_len
			){
			match_len++;
		}
		if(match_len == str2_len){
			return i;
		}
	}
	return -1;
}
int main(int argc, char const *argv[])
{
	const char* str1 = "ewfahatdmengoo";
	const char* str2 = "mengo";
	const char* str3 = StrStr(str1,str2);
	cout<<str3<<endl;
	cout<<StrLen(str3)<<endl;
	cout<<StrStrForLoop(str1,str2)<<endl;
	return 0;
}