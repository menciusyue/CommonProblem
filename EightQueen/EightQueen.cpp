/*
八皇后问题
*/
#include <stdio.h>
#include <iostream>
using namespace std;
//初始化
int place[8] = {0};
bool flag[8] = {1,1,1,1,1,1,1,1};
bool d1[15] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
bool d2[15] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
int number = 0;
void print(){
	int col,i,j;
	number++;
	cout<<"No."<<number<<endl;
	int table[8][8] = {0};
	for(col = 0;col < 8;col++){
		table[col][place[col]] = 1;
	}
	for(i = 0;i < 8;i++){
		for(j = 0;j < 8;j++){
			cout<<table[i][j]<<" ";
		}
		cout<<endl;
	}
}
void generate(int n){
	int col;
	for(col = 0;col < 8;col++){
		if(flag[col] && d1[n-col+7] && d2[n+col]){
			place[n] = col;
			flag[col] = 0;
			d1[n-col+7] = 0;
			d2[n+col] = 0;
			if(n < 7){
				generate(n+1);
			}
			else{
				print();
			}
			//回溯
			flag[col] = 1;
			d1[n-col+7] = 1;
			d2[n+col] = 1;
		}
	}
}

int main(int argc, char const *argv[])
{
	generate(0);
	return 0;
}