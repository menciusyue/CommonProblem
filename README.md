# Git

#### Git全局设置

```shell
git config --global user.name "MMMMMZY"
git config --global user.email "1057964113@qq.com"
```

#### 创建Git仓库

```shell
mkdir data_structure_and_algorithm
cd data_structure_and_algorithm
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/menciusyue/data_structure_and_algorithm.git
git push -u origin master
```

#### 已有仓库

```shell
cd existing_git_repo
git remote add origin https://gitee.com/menciusyue/data_structure_and_algorithm.git
git push -u origin master
```

