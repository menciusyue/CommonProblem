/*
排列组合
abc
acb
bac
bca
cba
cab
*/
#include <iostream>
using namespace std;
void permutation(char* p,const int k,const int m){
	if(k == m){
		for(int i = 0;i <= m;i++){
			cout<<p[i];
		}
		cout<<endl;
	}
	else{
		for(int i = k;i <= m;i++){
			swap(p[k],p[i]);
			permutation(p,k+1,m);
			swap(p[k],p[i]);
		}
	}
}
int  main(int argc, char const *argv[])
{
	char c[] = "abc";
	permutation(c,0,2);
	return 0;
}