//用数组解决约瑟夫环问题
#include <iostream>
using namespace std;
int JCArr(int num,int step){
	int arr[num];
	for (int i = 1; i <= num; ++i){
		arr[i-1] = i;
	}
	int n = num,i = 0,curOut = 1;
	while(num != 1){
		if(arr[i] == -1){
			i++;
		}
		else{
			i++;
			curOut++;
		}
		if(i == n){ 	//超过末尾从头开始
			i = 0;
		}
		if(curOut == step && arr[i] != -1){
			cout<<"deleting "<<arr[i]<<endl;
			arr[i] = -1;
			i++;
			if(i == n){		//这里也有可能发生越界，小心呀！
				i = 0;
			}
			curOut = 1;
			num--;	//别忘了总人数减一 
		}
	}
	int k = 0;
	for(;k < n;k++){
		if(arr[k] != -1)
			break;
	}
	return arr[k];
}
int main(int argc, char const *argv[])
{
	cout<<JCArr(6,3)<<endl;
	return 0;
}
